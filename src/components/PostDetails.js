import React from 'react';
import {Carousel, Glyphicon, Button} from 'react-bootstrap';
import {Col} from 'react-bootstrap';
import love from '../love.png';
import logo1 from '../1.png'

class PostDetails extends React.Component {
    
    state = {
        index: this.props.id,
        direction: null,
        comment: '',
        isEdit: false,
        editInfo: ''
    };
    
    toggleLike = (id, likeState) =>{
        likeState
            ? this.props.likeCount(-1, id, false)
            : this.props.likeCount(1, id, true)
    }

    handleSelect = (selectedIndex, e) => {
        this.setState({
            index: selectedIndex,
            direction: e.direction
        });
    }

    isEditInfo = () => {
        this.setState({
            isEdit : true
        });
    }

    handleEditChange = (e) => {
        this.setState({ editInfo: e.target.value });
    }

    editKeyPress = (e) => {
        if(e.keyCode === 13 && e.target.value){
           this.props.editInfo(this.state.index, e.target.value)
           this.setState({
                isEdit: false,
                editInfo: ''
           })
        }
     }

    handleChange = (e) => {
        this.setState({ comment: e.target.value });
    }
  
    keyPress = (e) => {
        if(e.keyCode === 13 && e.target.value){
           this.props.addComment(this.state.index, e.target.value)
           this.setState({
               comment:''
           })
        }
     }

    render() {
      const { index, direction } = this.state;
  
      return (
        <Carousel
          indicators={false}
          activeIndex={index}
          direction={direction}
          onSelect={this.handleSelect}
        >
          {
            this.props.posts.map((post, index)=>
                <Carousel.Item key={index}>
                    <Col md={7} className="p0">
                        <img className="car-style-image"
                            alt="535x600" 
                            src={post.Image} />
                    </Col>
                    <Col md={5} className="p0">
                        <div className="car-style-details">
                            <div className="border-bottom">
                                <div className="edit-info">
                                    <img alt="prof"
                                        className="prof-img"
                                        src={logo1}/>
                                    <span className="usr-info"> Username </span>
                                    <div>
                                        {
                                        this.state.isEdit 
                                            ? <input 
                                                className="label-input-style"
                                                value={this.state.editInfo}
                                                onChange={this.handleEditChange} 
                                                onKeyDown={this.editKeyPress} 
                                                placeholder="Add Label..."/>
                                            : <span className="usr-info">{post.label}</span>
                                        }
                                        <Button
                                            className="usr-edit-info"
                                            onClick={this.isEditInfo}
                                            bsSize="xsmall">
                                            {post.label
                                            ? <Glyphicon glyph="pencil"/>
                                            : <Glyphicon glyph="plus"/>}
                                            
                                        </Button>
                                    </div>
                                </div>
                                <Button 
                                    className="trash-style"
                                    onClick={() => this.props.deletePost(index)}
                                    bsSize="large">
                                    <Glyphicon glyph="trash" />
                                </Button>
                            </div>
                            <div className="comment-box border-bottom">
                               { 
                                post.comments
                                ? post.comments.map((comment, i)=>
                                    <div key={i} className="comment-style">
                                        <span className="comment">{comment}</span>
                                        <Glyphicon className="del-comment-style"
                                            onClick={() => this.props.deleteComment(index, i)}
                                            glyph="remove" />
                                    </div>
                                )
                                : null 
                                }
                            </div>
                            <div className="border-bottom">
                                <img alt="likes"
                                    src={love} 
                                    style={post.likeState
                                        ? {backgroundColor: 'red', border: '3px solid red'} 
                                        : {backgroundColor: 'gray', border: '3px solid gray'}}
                                    onClick={()=>this.toggleLike(index, post.likeState)}
                                    className="like-icon"/> 
                                <p><b>{post.likes} likes</b></p>

                            </div>
                            <div>
                                <input 
                                    className="comment-input-style"
                                    value={this.state.comment}
                                    onChange={this.handleChange} 
                                    onKeyDown={this.keyPress} 
                                    placeholder="Add a comment..."/>
                            </div>
                        </div>
                    </Col>
                </Carousel.Item> 
            )
        }
          
        </Carousel>
      );
    }
  }
  
  export default PostDetails;